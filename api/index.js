export default (axios) => {
  return {
    /**
     * Get projects
     * @param {*} params
     * @return {promise} promises
     */
    getProjects() {
      return axios.$get(`/projects-manage/index`).then(response => {
        return Promise.resolve(response)
      })
    },

    /**
     * Get project info
     * @param {*} params
     * @return {promise} promises
     */
    getProjectInfo(params) {
      return axios.$get(`/projects-manage/index/${params.projectId}`
      ).then(response => {
        return Promise.resolve(response)
      })
    },

    /**
    * Update project info
    * @param {*} params
    * @return {promise} promises
    */
    updateProjectInfo(params) {
      let formData = new FormData();
      formData.set('name', params.name);

      return axios.$post(`/projects-manage/update?id=${params.projectId}`, formData).then(response => {
        return Promise.resolve(response)
      })
    }
  }
}
