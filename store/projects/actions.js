import api from './../../api'

export default {
    getList({commit}, params) {
        return api(this.$axios).getProjects(this.$axios, params).then(data => {
            commit('setProjects', (data.projects || []))
        })
    },

    getInfo({commit}, params) {
        return api(this.$axios).getProjects(params).then(data => {
            let project = (data.projects && data.projects.length>0) ? data.projects[0] : null
            commit('setProject', project)

            return project
        })
    },

    updateInfo({commit}, params) {
        return api(this.$axios).updateProjectInfo(params).then(data => {
            commit('setProject', data.project)
        })
    },
}
