export default {
    list(state) {
        return state.projects
    },

    info(state) {
        return state.project
    }
}
