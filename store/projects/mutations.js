export default {
    setProjects(state, data) {
        state.projects = data
    },

    setProject(state, data) {
        state.project = data
    }
}
